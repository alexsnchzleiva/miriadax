package com.example.localizacion;

import java.util.List;

import android.app.Activity;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity implements LocationListener {

	private static final String[] A = { "n/d", "preciso", "impreciso" };
	private static final String[] P = { "n/d", "bajo", "medio", "alto" };
	private static final String[] E = { "fuera de servicio",
			"temporalmente no disponible ", "disponible" };
	private LocationManager manejador;
	private TextView salida;
	private String proveedor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		salida = (TextView) findViewById(R.id.salida);
		manejador = (LocationManager) getSystemService(LOCATION_SERVICE);
		log("Proveedores de localización: \n");
		muestraProveedores();
		Criteria criteria = new Criteria();
		proveedor = manejador.getBestProvider(criteria, true);
		log("Mejor proveedor: " + proveedor + "\n");
		log("Comenzamos con la última localización conocida:");
		Location localizacion = manejador.getLastKnownLocation(proveedor);
		muestraLocaliz(localizacion);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	protected void onResume(){
		manejador.requestLocationUpdates(proveedor, 10000, 1, this);
	}
	
	@Override
	protected void onPause(){
		manejador.removeUpdates(this);
	}
	

	@Override
	public void onLocationChanged(Location arg0) {
		log("Nueva localización: ");
		muestraLocaliz(arg0);
	}

	@Override
	public void onProviderDisabled(String arg0) {
		log("Proveedor deshabilitado: " + proveedor + "\n");
	}

	@Override
	public void onProviderEnabled(String arg0) {
		log("Proveedor habilitado: " + proveedor + "\n");
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		log("Cambia estado proveedor: " + proveedor + ", estado=" + E[Math.max(0,arg1)] + ", extras=" + arg2 +"\n");
	}
	
	
	private void log(String cadena){
		salida.append(cadena + "\n");
	}
	
	private void muestraLocaliz(Location localizacion){
		if(localizacion == null){
			log("Localización desconocida\n");
		}
		else{
			log(localizacion.toString() + "\n");
		}
	}
	
	private void muestraProveedores(){
		List<String> proveedores = manejador.getAllProviders();
		for(String proveedor:proveedores){
			muestraProveedor(proveedor);
		}
	}
	
	private void muestraProveedor(String cadena){
		LocationProvider info = manejador.getProvider(cadena);
		log("LocationProvider[ "+"getName=" + info.getName()+
                ", isProviderEnabled=" + 
                    manejador.isProviderEnabled(proveedor)+ 
                ", getAccuracy=" + A[Math.max(0, info.getAccuracy())]+
                ", getPowerRequirement=" +
                    P[Math.max(0, info.getPowerRequirement())]+
                ", hasMonetaryCost=" + info.hasMonetaryCost()+
                ", requiresCell=" + info.requiresCell()+
                ", requiresNetwork=" + info.requiresNetwork()+
                ", requiresSatellite=" + info.requiresSatellite()+
                ", supportsAltitude=" + info.supportsAltitude()+
                ", supportsBearing=" + info.supportsBearing()+
                ", supportsSpeed=" + info.supportsSpeed()+" ]\n");
	}

}
