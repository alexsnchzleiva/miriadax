package com.estado;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Estado extends Activity {
	
	private TextView textView;
	String cadena = new String();
	private Button boton;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.estado);
		
		boton = (Button) findViewById(R.id.boton);
		textView = (TextView) findViewById(R.id.texto);
		
//		if(savedInstanceState != null){
//			cadena = savedInstanceState.getString("cadena");
//			textView.setText(cadena);
//		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.estado, menu);
		return true;
	}
	
	@Override
	protected void onSaveInstanceState(Bundle savedInstanceState){
		super.onSaveInstanceState(savedInstanceState);
		savedInstanceState.putString("cadena", cadena);
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState){
		super.onRestoreInstanceState(savedInstanceState);
		cadena = savedInstanceState.getString("cadena");
		textView.setText(cadena);
	}
	
	public void contador(View view){
		cadena += "A";
		textView.setText(cadena);
	}

}
