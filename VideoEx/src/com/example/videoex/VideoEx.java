package com.example.videoex;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoEx extends Activity {
	
	private VideoView mVideoView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video_ex);
		mVideoView = (VideoView) findViewById(R.id.surface_view);
		mVideoView.setVideoPath("/mnt/sdcard/video.mp4");
		mVideoView.setMediaController(new MediaController(this));
		mVideoView.start();
		mVideoView.requestFocus();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.video_ex, menu);
		return true;
	}

}
