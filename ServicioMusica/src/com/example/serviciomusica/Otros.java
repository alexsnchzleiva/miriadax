package com.example.serviciomusica;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.widget.Toast;

public class Otros extends Service {
	
	private NotificationManager nm;
	
	@Override
	public void onCreate() {
		nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE); 
	}

	@Override
	public int onStartCommand(Intent intenc, int flags, int idArranque) {
		Notification notificacion = new Notification(R.drawable.ic_launcher, "OOOOO",
                System.currentTimeMillis() );
		
		PendingIntent intencionPendiente = PendingIntent.getActivity(
		          this, 0, new Intent(this, MainActivity.class), 0);
		notificacion.setLatestEventInfo(this, "Socorro",
		       "información Socooro ya", intencionPendiente);
		notificacion.defaults |= Notification.DEFAULT_SOUND; 
		notificacion.defaults |= Notification.DEFAULT_VIBRATE;
		notificacion.defaults |= Notification.DEFAULT_LIGHTS;
		nm.notify(2, notificacion); 
		
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		nm.cancel(2);
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

}
