package com.asteroides;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class AcercaDe extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.acercade);
		Bundle extras = getIntent().getExtras();
		String nombre = extras.getString("nombre");
		Log.d(nombre, nombre);
		
		Intent intent = new Intent();
		intent.putExtra("resultado", "Valor resultado");
		setResult(RESULT_OK,intent);
	}
	
}
