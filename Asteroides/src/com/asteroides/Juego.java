package com.asteroides;

import android.app.Activity;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;


public class Juego extends Activity {
	
	private VistaJuego vistaJuego;
	MediaPlayer mp;
	

	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.juego);
		vistaJuego = (VistaJuego) findViewById(R.id.vistaJuego1);
		vistaJuego.setPadre(this);
		}
	
	@Override
	protected void onPause(){
		super.onPause();
		vistaJuego.getThread().pausar();
		vistaJuego.getmSensorManager().unregisterListener(vistaJuego);
	}
	
	@Override 
	protected void onResume() {
		super.onResume();
		vistaJuego.getThread().reanudar();
		vistaJuego.getmSensorManager().registerListener(vistaJuego, vistaJuego.mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
	}
		 
	@Override protected void onDestroy() {
		vistaJuego.getThread().detener();
	    super.onDestroy();
	}
	
}
