package com.asteroides;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {

	private String prueba;
	private Button bAcercaDe;
	private Button bSalir;
	public static AlmacenPuntuaciones almacen = new AlmacenPuntuacionesArray();
	MediaPlayer mp;
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        bAcercaDe = (Button) findViewById(R.id.button3);
        bSalir = (Button) findViewById(R.id.button4);
        
        bAcercaDe.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View view) {
                 lanzarAcercaDe(null);
                 startService(new Intent(MainActivity.this, ServicioMusica.class));
           }     });
       
        bSalir.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View view) {
                 salir(null);
           }     });
        
        mp = MediaPlayer.create(this, R.raw.audio);
        almacen = new AlmacenPuntuacionesFicheroInterno(this);
        
        Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show();
    }

    
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        
        return true;
    }
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
			case R.id.acercaDe: lanzarAcercaDe(null);
				break;
			case R.id.config: lanzarPreferencias(null);
				break;
		}
		
		return true;
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode==1234 & resultCode==RESULT_OK & data!=null) {
			int puntuacion = data.getExtras().getInt("puntuacion");
			String nombre = "Yo";
			almacen.guardarPuntuaciones(puntuacion, nombre, System.currentTimeMillis());
			lanzarPuntuaciones(null);
		}
	}
	
	
	public void jugar(View view){
		Intent intent = new Intent(this, Juego.class);
		startActivity(intent);
	}
    
    public void lanzarAcercaDe(View view){
    	Intent intent = new Intent(this,AcercaDe.class);
    	intent.putExtra("nombre", "sanchezLeiva");
    	//startActivity(intent);
    	startActivityForResult(intent, 1234);
    }
    
    public void lanzarPreferencias(View view){
    	Intent intent = new Intent(this,Preferencias.class);
    	startActivity(intent);
    }
    
    
    public void salir(Object object) {
    	finish();
	}
    
    public void lanzarPuntuaciones(View view){
    	Intent intent = new Intent(this,Puntuaciones.class);
    	startActivity(intent);
    }
    
    @Override 
    protected void onStart() {
       super.onStart();
   	   Toast.makeText(this, "onStart", Toast.LENGTH_SHORT).show();
   	}
    	 
	@Override 
	protected void onResume() {
	   super.onResume();
	   mp.start();
	   Toast.makeText(this, "onResume", Toast.LENGTH_SHORT).show();
	}
	 
	@Override 
	protected void onPause() {
	   mp.pause();
	   Toast.makeText(this, "onPause", Toast.LENGTH_SHORT).show();
	   super.onPause();
	}
	 
	@Override 
	protected void onStop() {
	   Toast.makeText(this, "onStop", Toast.LENGTH_SHORT).show();
	   super.onStop();
	   }
	 
	@Override 
	protected void onRestart() {
	   super.onRestart();
	   Toast.makeText(this, "onRestart", Toast.LENGTH_SHORT).show();
	}
	 
	@Override 
	protected void onDestroy() {
	   Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show();
	   stopService(new Intent(MainActivity.this,
				ServicioMusica.class));
	   super.onDestroy();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle estadoGuardado) {
		super.onSaveInstanceState(estadoGuardado);
		if (mp != null) {
			int pos = mp.getCurrentPosition();
			estadoGuardado.putInt("posicion", pos);
		}
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle estadoGuardado) {
		super.onRestoreInstanceState(estadoGuardado);
		if (estadoGuardado != null && mp != null) {
			int pos = estadoGuardado.getInt("posicion");
			mp.seekTo(pos);
		}
	}
	
	public void lanzaJuego(View view) {
        Intent i = new Intent(this, Juego.class);
        startActivityForResult(i, 1234);
  }
    
}
