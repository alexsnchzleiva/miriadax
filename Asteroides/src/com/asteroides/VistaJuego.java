package com.asteroides;

import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;


public class VistaJuego extends View implements SensorEventListener {

	private Activity padre;
	private Vector<Grafico> Asteroides;
	private int numAsteroides = 5;
	private int numFragmentos = 3;
	private Grafico nave;
	private int giroNave;
	private float aceleracionNave;
	private static final int PASO_GIRO_NAVE = 5;
	private static final float PASO_ACELERACION_NAVE = 0.5F;
	private ThreadJuego thread = new ThreadJuego();
	private static int PERIODO_PROCESO = 50;
	private long ultimoProceso = 0;
	private float mX=0, mY=0;
	private boolean disparo=false;
	private boolean hayValorInicial = false;
    private float valorInicial;
    private Grafico misil;
    private static int PASO_VELOCIDAD_MISIL = 12;private boolean misilActivo = false;
    private int tiempoMisil;
    private SensorManager mSensorManager;
    public Sensor mAccelerometer;
    private int puntuacion = 0;
	

	public VistaJuego(Context context, AttributeSet attrs) {
		super(context, attrs);
		Drawable drawableNave, drawableAsteroide, drawableMisil;
		drawableNave = context.getResources().getDrawable(R.drawable.nave);
		drawableAsteroide = context.getResources().getDrawable(R.drawable.asteroide1);
		
		ShapeDrawable dMisil = new ShapeDrawable(new RectShape());
		dMisil.getPaint().setColor(Color.WHITE);
		dMisil.getPaint().setStyle(Style.STROKE);
		dMisil.setIntrinsicHeight(3);
		dMisil.setIntrinsicWidth(15);
		drawableMisil = dMisil;
		
	    setmSensorManager((SensorManager) context.getSystemService(Context.SENSOR_SERVICE));
	    mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		List<Sensor> listSensor = getmSensorManager().getSensorList(Sensor.TYPE_ORIENTATION);
		
		if(!listSensor.isEmpty()){
			Sensor orientationSensor = listSensor.get(0);
			getmSensorManager().registerListener(this, orientationSensor, SensorManager.SENSOR_DELAY_GAME);
		}
		
		nave = new Grafico(this, drawableNave);
		Asteroides = new Vector<Grafico>();
		
		for(int i=0;i < numAsteroides; i++){
			Grafico asteroide = new Grafico(this, drawableAsteroide);
			asteroide.setIncX(Math.random() * 4 -2);
			asteroide.setIncY(Math.random() * 4 -2);
			asteroide.setAngulo((int) Math.random() * 360);
			asteroide.setRotacion((int) Math.random() * 8 -4);
			Asteroides.add(asteroide);
		}
		
	}
	
	class ThreadJuego extends Thread {
		
		private boolean pausa, corriendo;
		
		
		public synchronized void pausar (){
			pausa = true;
		}
		
		public synchronized void reanudar(){
			pausa = false;
			notify();
		}
		
		public void detener(){
			corriendo = false;
			
			if(pausa){
				reanudar();
			}
			
		}
		
	   @Override
	   public void run() {
		   corriendo = true;
		   
	          while (true) {
	                 actualizarFisica();
	                 
	                 synchronized(this){ 
	                	 while(pausa){
	                		 try{
	                			 wait();
	                		 }
	                		 catch(Exception e){}
	                	 }
	                 }
	          }
	   }
	   
	}
	
	
	@Override
	protected void onSizeChanged(int ancho, int alto, int anchoAnterior, int altoAnterior){
		super.onSizeChanged(ancho, alto, anchoAnterior, altoAnterior);
		
		for(Grafico asteroide:Asteroides){
			do{
				asteroide.setPosX(Math.random()*(ancho-asteroide.getAncho()));
			    asteroide.setPosY(Math.random()*(alto-asteroide.getAlto()));
			} while(asteroide.distancia(nave) < (ancho+alto)/5);
		}
		
		nave.setPosX(100);
		nave.setPosY(200);	
		
		ultimoProceso = System.currentTimeMillis();
		thread.start();
	}
	
	
	@Override
	protected synchronized void onDraw(Canvas canvas){
		super.onDraw(canvas);
		
		for(Grafico asteroide:Asteroides){
			asteroide.dibujarGrafico(canvas);
		}
		
		if (misilActivo==true){
			misil.dibujarGrafico(canvas); 
			}
		
		nave.dibujarGrafico(canvas);
		
	}
	
	
	protected synchronized void actualizarFisica(){
		long ahora = System.currentTimeMillis();
		
		if(ultimoProceso + PERIODO_PROCESO > ahora){
			return;
		}
		
		double retardo = (ahora - ultimoProceso) / PERIODO_PROCESO;
		ultimoProceso = ahora;
		nave.setAngulo((int) (nave.getAngulo() + giroNave * retardo));
		double nIncX = nave.getIncX() + aceleracionNave * Math.cos(Math.toRadians(nave.getAngulo())) * retardo;
		double nIncY = nave.getIncY() + aceleracionNave * Math.sin(Math.toRadians(nave.getAngulo())) * retardo;
		
		if (Math.hypot(nIncX,nIncY) <= Grafico.getMaxVelocidad()){
            nave.setIncX(nIncX);
            nave.setIncY(nIncY);
		}
		
		for (Grafico asteroide : Asteroides) {
            asteroide.incrementarPos(retardo);
		}
		
		if (misilActivo) {
		       misil.incrementarPos(retardo);
		       tiempoMisil-=retardo;
		       if (tiempoMisil < 0) {
		             misilActivo = false;
		       } else {
		for (int i = 0; i < Asteroides.size(); i++)
		             if (misil.verificaColision(Asteroides.elementAt(i))) {
		                    destruyeAsteroide(i);
		                    break;
		             }
		       }
		}
		
		for (Grafico asteroide : Asteroides) {
			if (asteroide.verificaColision(nave)) {
			       salir();
			}
		}
		
	}
	
	public boolean ontouchEvent(MotionEvent event){
		super.onTouchEvent(event);
		float x = event.getX();
		float y = event.getY();
		
		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				disparo = true;
				break;
				
			case MotionEvent.ACTION_MOVE:
				float dx = Math.abs(x - mX);
				float dy = Math.abs(y - mY);
				
				if (dy < 10 && dx > 10) {
					giroNave = Math.round((x - mX) / 2);
					disparo = false;
				} else if (dx < 10 && dy > 10) {
					aceleracionNave = Math.round((mY - y) / 15);
					disparo = false;
				}
				break;
				
			case MotionEvent.ACTION_UP:
				giroNave = 0;
				aceleracionNave = 0;
				if (disparo) {
					activaMisil();
				}
				break;
		}
		
		mX = x;
		mY = y;
		return true;
	}


	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
	}


	@Override
	public void onSensorChanged(SensorEvent arg0) {
		float valor = arg0.values[1];
		
		if(!hayValorInicial){
			valorInicial = valor;
			hayValorInicial = true;
		}
		
		giroNave = (int) (valor - valorInicial) / 3;
	}
	
	private void destruyeAsteroide(int i) {
	       Asteroides.remove(i);
	       puntuacion += 1000;
	       misilActivo = false;
	       
	       if (Asteroides.isEmpty()) {
               salir();
	       }
	}
	
	private void activaMisil() {
	       misil.setPosX(nave.getPosX()+ nave.getAncho()/2-misil.getAncho()/2);
	       misil.setPosY(nave.getPosY()+ nave.getAlto()/2-misil.getAlto()/2);
	       misil.setAngulo(nave.getAngulo());
	       misil.setIncX(Math.cos(Math.toRadians(misil.getAngulo())) *
	                        PASO_VELOCIDAD_MISIL);
	       misil.setIncY(Math.sin(Math.toRadians(misil.getAngulo())) *
	                        PASO_VELOCIDAD_MISIL);
	       tiempoMisil = (int) Math.min(this.getWidth() / Math.abs( misil.
	          getIncX()), this.getHeight() / Math.abs(misil.getIncY())) - 2;
	       misilActivo = true;
	}


	public ThreadJuego getThread() {
		return thread;
	}


	public void setThread(ThreadJuego thread) {
		this.thread = thread;
	}


	SensorManager getmSensorManager() {
		return mSensorManager;
	}


	void setmSensorManager(SensorManager mSensorManager) {
		this.mSensorManager = mSensorManager;
	}
	
	public void setPadre(Activity padre) {
		this.padre = padre;
	}
	
	private void salir() {
		Bundle bundle = new Bundle();
		bundle.putInt("puntuacion", puntuacion);
		Intent intent = new Intent();
		intent.putExtras(bundle);
		padre.setResult(Activity.RESULT_OK, intent);
		padre.finish();
		}
	

}

