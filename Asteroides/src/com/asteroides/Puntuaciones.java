package com.asteroides;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class Puntuaciones extends ListActivity {
	
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.puntuaciones);
		setListAdapter(
				//new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, MainActivity.almacen.listaPuntuaciones(10)));
				//new ArrayAdapter<String>(this, R.layout.elemento_lista, R.id.titulo, MainActivity.almacen.listaPuntuaciones(10)));
				new MiAdaptador(this, MainActivity.almacen.listaPuntuaciones(10)));
	}
	
	
	@Override
	protected void onListItemClick(ListView listView, View view, int position, long id){
		super.onListItemClick(listView, view, position, id);
		Object object = getListAdapter().getItem(position);
		Toast.makeText(this, "Selección: " + Integer.toString(position) +  " - " + object.toString(), Toast.LENGTH_LONG).show();
	}

}
