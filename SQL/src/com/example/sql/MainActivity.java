package com.example.sql;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Almacen almacen = new Almacen(this, "DBUsuarios", null, 1);
		SQLiteDatabase db = almacen.getWritableDatabase();
		
		if(db != null){
			for(int i=0;i<=5;i++){
				int codigo = i;
				String nombre = "Usuario " + i;
				db.execSQL("INSERT INTO Usuarios (codigo,nombre) VALUES (" + codigo + ", '" + nombre + "')");
			}
			
			Cursor cursor = db.rawQuery("SELECT codigo, nombre FROM Usuarios", null);
			
			while(cursor.moveToNext()){
				String a = cursor.getColumnName(0);
				String b = cursor.getString(0);
				String c = cursor.getString(1);
				Log.d("DB", cursor.getString(1));
			}
			
			cursor.close();
			db.close();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
