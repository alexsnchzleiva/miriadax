package com.sd;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	TextView textViewEstado;
	TextView textViewDirectorio;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		
		textViewEstado = (TextView) findViewById(R.id.estado);
		String estadoSD = Environment.getExternalStorageState();
		textViewEstado.setText(estadoSD);
		
		textViewDirectorio = (TextView) findViewById(R.id.directorio);
		File directorioSD = Environment.getExternalStorageDirectory();
		textViewDirectorio.setText(directorioSD.toString());
		
		if(estadoSD.equals(Environment.MEDIA_MOUNTED)){
			try {
				FileOutputStream fos = new FileOutputStream(Environment.getExternalStorageDirectory() + "/pruebaSD.txt", true);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		InputStream is = getResources().openRawResource(R.drawable.ic_launcher);
		
		return true;
	}

}
