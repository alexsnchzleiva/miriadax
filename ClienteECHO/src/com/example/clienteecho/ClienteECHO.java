package com.example.clienteecho;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

public class ClienteECHO extends Activity {

	private TextView output;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cliente_echo);

		output = (TextView) findViewById(R.id.TextView01);
		ejecutaCliente();
	}

	private void ejecutaCliente() {
		String ip = "158.42.146.127";
		int puerto = 7;
		
		try{
			Socket socket = new Socket(ip, puerto);
			BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter salida = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
			salida.println("Hola Mundo");
			log(entrada.readLine());
			socket.close();
		}
		catch(Exception e){
			log(e.getMessage());
		}
	}
	
	private void log(String string){
		output.append(string + "\n");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.cliente_echo, menu);
		return true;
	}

}
